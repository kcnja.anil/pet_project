import random


class Pet():
    boredom_decrement=2

    def __init__(self, pet_name):  # initialize instance variables
        self.hunger=random.randrange(0,5)
        self.boredom=random.randrange(0,5)
        self.pet_name=pet_name
        self.sound=[]

    def clock_tick(self):  # method that increments time for every iteration
        self.boredom +=1  # boredom and hunger is from 1 to 5
        self.hunger +=1

    def __str__(self):  # method to produce string representation of pet's current state
        if self.boredom>5:
            bored= "BORED"
        else:
            bored="NOT BORED"
        if self.hunger>5:
            hunger="HUNGRY"
        else:
            hunger="NOT HUNGRY"
        if bored=="NOT BORED" and hunger=="NOT HUNGRY":
            return (f"Your pet, {self.pet_name}, is HAPPY")
        return (f"Your pet, {self.pet_name}, is {bored} and {hunger}")

    def teach(self, new_word):  # method to teach pet new words and reduce boredom
        self.sound.append(new_word)
        self.reduce_boredom()

    def hi(self):  # method for pet to print a random word from list
        print(random.choice(self.sound))
        self.reduce_boredom()

    def reduce_boredom(self):  # method to reduce boredom
        self.boredom-=self.boredom_decrement
        if self.boredom < 0:
            self.boredom=0

    def feed(self):  # method used to feed the pet and relive hunger
        self.reduce_hunger()

    def reduce_hunger(self):  # method used to reduce hunger
        self.hunger=-1


pet_list = {}  # dictionary that stores instances
while True:
    adopt=input('Do you want to adopt a pet y/n? ')
    if adopt=='y':
        name_pet = input('Name your pet')
        pet_list[name_pet]=Pet(name_pet)  # saves instances with pet name as key
    else:
        name_pet=input("What is your pet's name? ")
    print('Hi there!\n These are the commands you need:\n1: to greet your pet\n2: to teach your pet a new word\n3: to feed your hungry pet\n4: EXIT')
    command=input('What do you want to do?')
    command=int(command)

    if command==1:
        pet_list[name_pet].hi()

    elif command==2:
        new_word = input('What new word to teach')
        pet_list[name_pet].teach(new_word)

    elif command==3:
        pet_list[name_pet].feed()
    else:
        exit()
    for k in pet_list.keys():  # loop to call clock_tick method for all instances
        pet_list[k].clock_tick()
    print(pet_list[name_pet])